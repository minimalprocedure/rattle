# rattle

## a nonsense declarative script for XML and HTML generation

convert rattle code:

```
KD: xml
KD: html

NS: _:http://example.org/myapp/
NS: xsi[
  schemaLocation=http://example.org/myapp/ http://example.org/myapp/schema.xsd
]:http://www.w3.org/2001/XMLSchema-instance
NS: dc:http://purl.org/dc/elements/1.1/
NS: dcterms:http://purl.org/dc/terms/

xxx.pluto[attr=pluto]:
  testo pluto
;;

xxx.pluto/pippo/paperino[attr=paperino]:
  testo di paperino dentro pippo
 ;;

xxx.pluto/pippo[attr=pippo]:
  testo pippo dentro pluto
;;
```
in xml:

```
<?xml version="1.0" ?>
<root xmlns:dcterms="http://purl.org/dc/terms/" xmlns:dc="http://purl.org/dc/elements/1.1/" xsi:schemaLocation="http://example.org/myapp/ http://example.org/myapp/schema.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://example.org/myapp/">
 <xxx:pluto attr="pluto">
  testo pluto
  <pippo>
   <paperino attr="paperino">
    testo di paperino dentro pippo
   </paperino>
   testo pippo dentro pluto
  </pippo>
 </xxx:pluto>
</root>
```

or rattle :


```
KD: html
KD: xml
KD: head

_.head/title:
  hello
;;

_.body:
;;

_.body/h1:
  titolo
;;

_.body/p:
  ciao mondo
;;
```
in HTML:

```
<html>
  <head>
    <title>hello</title>
  </head>
  <body>
    <h1>titolo</h1>
    <p>ciao mondo</p>
  </body>
</html>
```
