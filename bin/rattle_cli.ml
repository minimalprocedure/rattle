open Rattle

let infile = ref ""
let outfile = ref ""
let basetag = ref ""
let root = ref "root"
let force_xml = ref false
let force_html = ref false
let force_head = ref false
let usage = "rattle-cli usage:\n\n" ^ Sys.argv.(0) ^ " --in filename [options] \n"

let _args_parse =
  let opts =
    [ "--in", Arg.Set_string infile, ": rattle file"
    ; "--out", Arg.Set_string outfile, ": output to file"
    ; "--basetag", Arg.Set_string basetag, ": basetag"
    ; "--root", Arg.Set_string root, ": xml tag root"
    ; "--xml", Arg.Set force_xml, ": output xml"
    ; "--html", Arg.Set force_html, ": output html"
    ; "--head", Arg.Set force_head, ": output html head"
    ]
  in
  let anons _ = () in
  Arg.parse opts anons usage
;;

let run () =
  let source = Utilities.File_h.open_file !infile in
  let codes =
    if !force_xml && !force_html && !force_head
    then compile ~basetag:!basetag ~root:!root `ALL source
    else if !force_xml = false && !force_html = false && !force_head = false
    then compile ~basetag:!basetag ~root:!root `INTERNAL source
    else if !force_xml
    then compile ~basetag:!basetag ~root:!root `FORCE_XML source
    else if !force_html
    then compile ~basetag:!basetag ~root:!root `FORCE_HTML source
    else compile ~basetag:!basetag ~root:!root `FORCE_HEAD source
  in
  if String.trim !outfile = String.empty
  then
    List.iter
      (fun code ->
        let t, source = code in
        match t with
        | Kind.UNK ->
          Printf.eprintf "Unknown export, please try to force. \n";
          exit 1
        | Kind.HEAD -> Printf.printf "HEAD\n====\n%s" source
        | Kind.HTML -> Printf.printf "HTML\n====\n%s" source
        | Kind.XML -> Printf.printf "XML\n====\n%s" source)
      codes
  else
    List.iter
      (fun code ->
        let t, _source = code in
        match t with
        | Kind.UNK ->
          Printf.eprintf "Unknown export, please try to force. \n";
          exit 1
        | _ -> save_code !outfile code)
      codes
;;

let () =
  if String.trim !infile = String.empty
  then (
    Printf.eprintf "please provide a source file. \n";
    exit 1)
  else run ()
;;
