(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

module String_h = struct
  let escape_dot str = Re.replace_string ~all:true (Re.Pcre.regexp "\\.+") ~by:"\\." str
end

module File_h = struct
  let open_file filename =
    let ic = In_channel.open_text filename in
    let source = In_channel.input_all ic in
    In_channel.close ic;
    source
  ;;

  let save_file filename source =
    let oc = Out_channel.open_text filename in
    Out_channel.output_string oc source;
    Out_channel.close oc
  ;;

  let save_code filename ext source =
    let fname = Printf.sprintf "%s%s" filename ext in
    save_file fname source
  ;;
end

module Regex_h = struct
  module Pcre = struct
    let regexp ?(caseless = true) ?flags regex_str =
      let cless_flag = if caseless then [ `CASELESS ] else [] in
      let flags = cless_flag @ Option.value ~default:[] flags in
      Re.Pcre.regexp ~flags regex_str
    ;;

    let all = Re.all
    let get_named_substring = Re.Pcre.get_named_substring
    let extract = Re.Pcre.extract
    let names = Re.Pcre.names
    let replace_string = Re.replace_string
    let replace = Re.replace
    let execp = Re.execp
    let get_group_opt = Re.Group.get_opt
    let get_group = Re.Group.get
    let matches = Re.matches
    let scan_source rex_attrs source = all (regexp rex_attrs) source

    let prepare_rex pre ops =
      List.map (fun op -> Printf.sprintf {|(%s%s)|} pre (String.trim op)) ops
      |> String.concat "|"
    ;;
  end
end
