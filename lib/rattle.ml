(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

module Soup = XSoup
open Utilities
open Soup
module Utilities = Utilities

module Attr = struct
  type t =
    { key : string
    ; value : string
    }
end

module Ns = struct
  type t =
    { ns : string option
    ; uri : string
    ; attrs : Attr.t list
    }
end

module Node = struct
  type t =
    { ns : string option
    ; tag : string
    ; attrs : Attr.t list
    ; text : string option
    }
end

module Res = struct
  type t =
    | ALL
    | NS of string
end

module Kind = struct
  type t =
    | HEAD
    | HTML
    | XML
    | UNK

  let map_of s =
    match String.uppercase_ascii s with
    | "HEAD" -> HEAD
    | "HTML" -> HTML
    | "XML" -> XML
    | _ -> UNK
  ;;

  let ext_of = function
    | HEAD -> ".head.html"
    | HTML -> ".html"
    | XML -> ".xml"
    | UNK -> ".unk"
  ;;
end

let getkinds source =
  let rex = {|KD:\s+(\w+)|} in
  List.fold_left
    (fun kinds matches ->
      let part = Regex_h.Pcre.get_group_opt matches in
      let kind = Option.value ~default:"" (part 1) in
      Kind.map_of kind :: kinds)
    []
    (Regex_h.Pcre.scan_source rex source)
;;

let attributes ~prefix source =
  let rex_attrs = {|([^;\n\]\[]+)=([^;\n\]\[]+)|} in
  let source = String.trim source in
  List.fold_left
    (fun attrs matches ->
      let part = Regex_h.Pcre.get_group_opt matches in
      match part 1 with
      | None -> attrs
      | Some key ->
        let key = String.trim key in
        let key =
          Option.fold
            ~none:key
            ~some:(fun prefix -> Printf.sprintf "%s:%s" prefix key)
            prefix
        in
        let value = Option.value ~default:"" (part 2) |> String.trim in
        Attr.{ key; value } :: attrs)
    []
    (Regex_h.Pcre.scan_source rex_attrs source)
  |> List.rev
;;

let attributes_assoc attrs =
  List.fold_left
    (fun attrs attr ->
      let Attr.{ key; value } = attr in
      (key, value) :: attrs)
    []
    attrs
;;

let namespaces source =
  let rex_ns = {|NS:\s+(\w+)(\[\s*(.*)\s*\])*:(.*)|} in
  let source = String.trim source in
  List.fold_left
    (fun namespaces matches ->
      let part = Regex_h.Pcre.get_group_opt matches in
      match part 1, part 4 with
      | Some ns, Some uri ->
        let ns = if ns = "_" then None else Some ns in
        let attrs =
          Option.fold ~none:[] ~some:(fun attrs -> attributes ~prefix:ns attrs) (part 3)
        in
        Ns.{ ns; uri; attrs } :: namespaces
      | _ -> namespaces)
    []
    (Regex_h.Pcre.scan_source rex_ns source)
;;

let namespaces_to_assoc ?prefix namespaces =
  List.fold_left
    (fun assoc ns ->
      let Ns.{ ns; uri; attrs } = ns in
      let attrs = attributes_assoc attrs in
      let assoc = attrs @ assoc in
      match prefix with
      | None ->
        let ns = Option.value ~default:"" ns in
        (ns, uri) :: assoc
      | Some prefix ->
        (match ns with
         | None -> (prefix, uri) :: assoc
         | Some ns -> (Printf.sprintf "%s:%s" prefix ns, uri) :: assoc))
    []
    namespaces
;;

let nodes ?(ns = Res.ALL) source =
  let source = String.trim source in
  let rex_nodes_by_ns ns =
    Printf.sprintf {|(%s)\.{1}([\w\/]+?)(\[.*\])*:([\W\S]+?)(?:;;)|} ns
  in
  let rex =
    match ns with
    | ALL -> rex_nodes_by_ns "\\w*?"
    | NS ns -> rex_nodes_by_ns ns
  in
  List.fold_left
    (fun nodes matches ->
      let part = Regex_h.Pcre.get_group_opt matches in
      match part 2 with
      | None -> nodes
      | Some tag ->
        let ns =
          Option.fold
            ~none:None
            ~some:(fun v -> if v = "_" then None else Some (String.trim v))
            (part 1)
        in
        let text =
          Option.fold
            ~none:None
            ~some:(fun v -> if v = "_" then None else Some (String.trim v))
            (part 4)
        in
        let attrs =
          Option.fold ~none:[] ~some:(fun attrs -> attributes ~prefix:None attrs) (part 3)
        in
        Node.{ ns; tag; attrs; text } :: nodes)
    []
    (Regex_h.Pcre.scan_source rex source)
  |> List.rev
;;

module Html = struct
  let hns_sep = "."
  let write_html = Soup.pretty_print
  let create_head () = Soup.create_element "head"

  let to_head ?html source =
    let html = Option.fold ~none:(Soup.parse "") ~some:(fun html -> html) html in
    let head =
      match html $? "head" with
      | None ->
        let head = create_element "head" in
        prepend_root html head;
        head
      | Some head -> head
    in
    let insert_ns () =
      List.iter
        (fun ns ->
          let Ns.{ ns; uri; attrs } = ns in
          let schema =
            Option.fold
              ~none:"schema"
              ~some:(fun ns -> Printf.sprintf "schema%s%s" hns_sep ns)
              ns
          in
          let attrs = attributes_assoc attrs in
          let link =
            create_element "link" ~attributes:([ "rel", schema; "href", uri ] @ attrs)
          in
          append_child head link)
        (namespaces source)
    in
    insert_ns ();
    let insert_nodes () =
      List.iter
        (fun node ->
          let Node.{ ns; tag; attrs; text } = node in
          let tag = Option.fold ~none:tag ~some:(fun ns -> ns ^ hns_sep ^ tag) ns in
          let attributes =
            List.fold_left
              (fun attrs attr ->
                let Attr.{ key; value } = attr in
                (key, value) :: attrs)
              []
              attrs
          in
          let attributes =
            ("name", tag) :: ("content", Option.value ~default:"" text) :: attributes
          in
          let meta = create_element "meta" ~attributes in
          append_child head meta)
        (nodes source)
    in
    insert_nodes ();
    html
  ;;

  let head_unwrap_strings head =
    Soup.fold (fun acc n -> Soup.pretty_print n :: acc) [] (Soup.children head)
  ;;
end

module Xml = struct
  let xns_sep = ":"
  let select_node parent tagname = Soup.select_one (String_h.escape_dot tagname) parent

  let parse_xml text =
    Markup.string text |> Markup.parse_xml |> Markup.signals |> Soup.from_signals
  ;;

  let create_xml ?(standalone = false) ~root ns =
    let alone = if standalone then {|standalone="yes"|} else "" in
    let xmlfmt = format_of_string {|<?xml version="1.0" %s ?>|} in
    let xml = parse_xml (Printf.sprintf xmlfmt alone) in
    let rootnode = Soup.create_element root ~attributes:ns in
    Soup.prepend_root xml rootnode;
    xml, rootnode
  ;;

  let write_xml ?(head = true) ?(standalone = false) soup =
    let source () =
      soup |> Soup.signals |> Markup.pretty_print |> Markup.write_xml |> Markup.to_string
    in
    if head
    then (
      let alone = if standalone then {|standalone="yes" |} else "" in
      let head = Printf.sprintf {|<?xml version="1.0" %s?>|} alone in
      Printf.sprintf "%s\n%s" head (source ()))
    else source ()
  ;;

  let append_node ?(attributes = []) parent tagname =
    match select_node parent tagname with
    | None ->
      let elt = Soup.create_element tagname ~attributes in
      Soup.append_child parent elt;
      elt
    | Some elt -> elt
  ;;

  let process_tag ?(attributes = []) ?text ~xml ~parent_node tagname =
    ignore xml;
    let insert_element tagname parent_node =
      let attributes =
        List.fold_left
          (fun attrs attr ->
            let Attr.{ key; value } = attr in
            (key, value) :: attrs)
          []
          attributes
      in
      append_node ~attributes parent_node tagname
    in
    let append_text elt text =
      let text = Option.value ~default:"" text in
      let textnode = Soup.create_text text in
      Soup.append_child elt textnode
    in
    let elt =
      Option.fold
        ~none:(insert_element tagname parent_node)
        ~some:(fun elt -> elt)
        (select_node parent_node tagname)
    in
    append_text elt text;
    elt
  ;;

  let rec process_all_tags ?(attributes = []) ?text ~xml ~parent_node tagnames =
    match tagnames with
    | [] -> ()
    | tagname :: [] -> process_tag ~attributes ?text ~xml ~parent_node tagname |> ignore
    | tagname :: tagnames ->
      (*let elt = process_tag ~attributes ?text ~xml ~parent_node tagname in*)
      let elt = append_node parent_node tagname in
      process_all_tags ~attributes ?text ~xml ~parent_node:elt tagnames
  ;;

  let to_xml ?(basetag = "") ?xml ~root source =
    let ns = namespaces source |> namespaces_to_assoc ~prefix:"xmlns" in
    let create_root root = Soup.create_element root ~attributes:ns in
    let xml, rootnode =
      Option.fold ~none:(create_xml ~root ns) ~some:(fun xml -> xml, create_root root) xml
    in
    (match select_node xml basetag with
     | None -> Soup.prepend_root xml rootnode
     | Some pnode -> Soup.prepend_child pnode rootnode)
    |> ignore;
    let insert_nodes () =
      List.iter
        (fun node ->
          let Node.{ ns; tag; attrs; text } = node in
          let tagname = Option.fold ~none:tag ~some:(fun ns -> ns ^ xns_sep ^ tag) ns in
          let tagparts = String.split_on_char '/' tagname in
          process_all_tags ~attributes:attrs ?text ~xml ~parent_node:rootnode tagparts)
        (nodes source)
    in
    insert_nodes ();
    xml
  ;;
end

let compile ?(basetag = "") ?(root = "root") ?xml ?html pass source =
  let write_xml () = Kind.XML, Xml.to_xml ~basetag ?xml ~root source |> Xml.write_xml in
  let write_html () =
    Kind.HTML, Xml.to_xml ~basetag ?xml ~root source |> Xml.write_xml ~head:false
  in
  let write_head () = Kind.HEAD, Html.to_head ?html source |> Html.write_html in
  match pass with
  | `INTERNAL ->
    let kinds = getkinds source in
    List.map
      (fun k ->
        match k with
        | Kind.HEAD -> write_head ()
        | Kind.HTML -> write_html ()
        | Kind.XML -> write_xml ()
        | Kind.UNK -> Kind.UNK, "Kind is unknown")
      kinds
  | `FORCE_XML -> [ write_xml () ]
  | `FORCE_HTML -> [ write_html () ]
  | `FORCE_HEAD -> [ write_head () ]
  | `ALL -> [ write_head (); write_html (); write_head () ]
  | _ -> [ Kind.UNK, "Unknown choice" ]
;;

let save_code filename code =
  let t, source = code in
  let fname = Printf.sprintf "%s%s" filename (Kind.ext_of t) in
  File_h.save_file fname source
;;

let save_all filename all = List.iter (fun code -> save_code filename code) all
